package com.volkov.xmlapp;



import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class MyErrorHandler implements org.xml.sax.ErrorHandler {

    public MyErrorHandler(){

    }

    @Override
    public void warning(SAXParseException exception) throws SAXException {
        System.out.println(exception.getLineNumber()+" "+exception.getMessage());
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        System.out.println(exception.getLineNumber()+" "+exception.getMessage());
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        System.out.println(exception.getLineNumber()+" "+exception.getMessage());
    }
}
