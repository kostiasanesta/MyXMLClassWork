<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml"/>
    <xsl:template match="/">
        <xsl:for-each select="BeerShop/Beer">

            <manufacturer>
                <xsl:value-of select="Manufacturer"/>
                <name>
                    <xsl:value-of select="Name"/>
                </name>
                <type>
                    <xsl:value-of select="Type"/>
                </type>
                <alchogol>
                    <xsl:value-of select="Al"/>
                </alchogol>
                <ingridients>
                    <xsl:apply-templates select="Ingridients">
                        <ingridient>
                            <xsl:value-of select="ingridient"/>
                        </ingridient>
                    </xsl:apply-templates>
                </ingridients>
                <chars>
                    <xsl:value-of select="Chars"/>
                </chars>
            </manufacturer>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>