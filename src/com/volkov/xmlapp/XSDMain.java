package com.volkov.xmlapp;


import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import com.volkov.xmlapp.parsers.SAXer;
import com.volkov.xmlapp.transforms.Transformator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import java.io.IOException;


public class XSDMain {

    public static void main(String[] args){
        String classPath = "src/com/volkov/xmlapp/";
        String filename = "Beer.xml";

        DOMParser parser = new DOMParser();
        parser.setErrorHandler(new MyErrorHandler());
        try {
            parser.setFeature("http://xml.org/sax/features/validation", true);
            parser.setFeature("http://apache.org/xml/features/validation/schema",true);
            parser.parse(classPath+filename);
        } catch (SAXNotRecognizedException e) {
            e.printStackTrace();
            System.out.print("идентификатор не распознан");
        } catch (SAXNotSupportedException e) {
            e.printStackTrace();
            System.out.println("неподдерживаемая операция");
        }catch (SAXException e) {
            e.printStackTrace();
            System.out.println("глобальная SAX ошибка");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO oshibka");
        }
        System.out.println("проверка "+filename+" завершена");
        //DOM parser
        //DOMingo doMingo = new DOMingo();
        //doMingo.runTheDOM(classPath+filename);
       // System.out.println(doMingo.getBeerShop().toString());
        //SAX parser
        SAXer saXer = new SAXer();
        saXer.saxThis(classPath+filename);
        System.out.println(saXer.getShop().toString()+saXer.getShop().getBeerList());
        Transformator transformator = new Transformator();
        transformator.transformMeth(classPath+"Transformer.xsl",classPath+filename,classPath+"newBeer.xml");
    }


}
