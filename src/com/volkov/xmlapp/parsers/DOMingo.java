package com.volkov.xmlapp.parsers;



import com.volkov.xmlapp.projectLogic.Beer;
import com.volkov.xmlapp.projectLogic.BeerShop;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class DOMingo {
    private DocumentBuilderFactory builderFactory;
    private DocumentBuilder documentBuilder;
    private Document document;
    private BeerShop beerShop;

    public BeerShop getBeerShop() {
        return beerShop;
    }

    public DOMingo() {
        builderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = builderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void runTheDOM(String link) {
        try {
            document = documentBuilder.parse(new File(link));
            //process();
            toObjective();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//na ekran
    private void process() {
        document.getDocumentElement().normalize();
        System.out.println("Root element " + document.getDocumentElement().getNodeName());
        NodeList nodeList = document.getElementsByTagName("Beer");
        System.out.println("_____________");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node childNode = nodeList.item(i);
            System.out.println("\n" + childNode.getNodeName());
            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) childNode;
                System.out.println("Name " + element.getElementsByTagName("Name").item(0).getTextContent());
                System.out.println("Type " + element.getElementsByTagName("Type").item(0).getTextContent());
                System.out.println("Alcochol " + element.getElementsByTagName("Al").item(0).getTextContent());
                System.out.println("Manufacturer " + element.getElementsByTagName("Manufacturer").item(0).getTextContent());
                System.out.print("Ingridients:");
                for (int l = 0; l < element.getElementsByTagName("ingridient").getLength(); l++) {
                    System.out.print("\t " + element.getElementsByTagName("ingridient").item(l).getTextContent());
                }
                System.out.println();
                System.out.println("Chars " + element.getElementsByTagName("Chars").item(0).getTextContent());
            }
        }
    }
//v objects sdelatb
    private void toObjective() {
        beerShop = new BeerShop();
        document.getDocumentElement().normalize();
        System.out.println("Root element " + document.getDocumentElement().getNodeName());
        NodeList nodeList = document.getElementsByTagName("Beer");
        System.out.println("_____________");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node childNode = nodeList.item(i);

            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) childNode;
                String[] ingridients = new String[element.getElementsByTagName("ingridient").getLength()];
                for (int l = 0; l < element.getElementsByTagName("ingridient").getLength(); l++) {
                   ingridients[l]=  element.getElementsByTagName("ingridient").item(l).getTextContent();
                }
                beerShop.getBeerList().add(new Beer(element.getElementsByTagName("Name").item(0).getTextContent(),
                        element.getElementsByTagName("Type").item(0).getTextContent(),
                        element.getElementsByTagName("Al").item(0).getTextContent(),
                        element.getElementsByTagName("Manufacturer").item(0).getTextContent(),
                        ingridients,
                        element.getElementsByTagName("Chars").item(0).getTextContent()));
            }
        }
    }
}
