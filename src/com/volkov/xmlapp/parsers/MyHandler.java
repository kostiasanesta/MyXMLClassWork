package com.volkov.xmlapp.parsers;


import com.volkov.xmlapp.projectLogic.Beer;
import com.volkov.xmlapp.projectLogic.BeerShop;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;



public class MyHandler extends DefaultHandler{
    private BeerShop beerShop;
    private Beer beer;
    private String thisElement;

    @Override
    public void startDocument() throws SAXException {
        beerShop = new BeerShop();
        System.out.println("Parse started");

    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("finish zdesb!");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equals("Beer")){
            beer = new Beer();
        }else {
            thisElement=qName;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("Beer")){
            beerShop.getBeerList().add(beer);
        }
        thisElement = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
       if(thisElement.equals("Name")){
           beer.setName(new String(ch,start,length));
       }else if(thisElement.equals("Type")){
           beer.setType(new String(ch,start,length));
       }else if(thisElement.equals("Al")){
           beer.setAlchogol(new String(ch,start,length));
       }else if(thisElement.equals("Manufacturer")){
           beer.setManufacturer(new String(ch,start,length));
       }else if(thisElement.equals("ingridient")){
           beer.getIngridients().getIngrList().add(new String(ch,start,length));
       }else if(thisElement.equals("Chars")){
           beer.setProcentege(new String(ch,start,length));
       }
    }

    public BeerShop getBeerShop() {
        return beerShop;
    }
}
