package com.volkov.xmlapp.parsers;



import com.volkov.xmlapp.projectLogic.BeerShop;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
import java.io.IOException;


public class SAXer extends DefaultHandler{
    private XMLReader xmlReader;
    private MyHandler handler;

        public SAXer(){
            handler = new MyHandler();
            try {
                xmlReader = XMLReaderFactory.createXMLReader();
                xmlReader.setContentHandler(handler);
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }

        public void saxThis(String link){
            try {
                xmlReader.parse(link);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }

        }

    public BeerShop getShop() {
        return  handler.getBeerShop();
    }
}
