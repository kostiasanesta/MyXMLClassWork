package com.volkov.xmlapp.projectLogic;




public class Beer {
    private String name;
    private String type;
    private String alchogol;
    private String manufacturer;
    private Ingridients ingridients;
    private String procentege;

    public Beer() {
        ingridients = new Ingridients();
    }

    public Beer(String name, String type, String alchogol, String manufacturer, String[] ingr, String procentege) {
        this.name = name;
        System.out.println(name);
        this.type = type;
        this.alchogol = alchogol;
        this.manufacturer = manufacturer;
        this.procentege = procentege;
        ingridients = new Ingridients(ingr);
        System.out.println("Beer gotof!");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAlchogol() {
        return alchogol;
    }

    public void setAlchogol(String alchogol) {
        this.alchogol = alchogol;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Ingridients getIngridients() {
        return ingridients;
    }

    public void setIngridients(Ingridients ingridients) {
        this.ingridients = ingridients;
    }

    public String getProcentege() {
        return procentege;
    }

    public void setProcentege(String procentege) {
        this.procentege = procentege;
    }

    @Override
    public String toString() {

        return "Beer{" +"\n"+
                "name='" + name + '\n' +
                ", type='" + type + '\n' +
                ", alchogol='" + alchogol + '\n' +
                ", manufacturer='" + manufacturer + '\n' +
                ", ingridients=" +  ingridients+"\n"+
                ", procentege='" + procentege + '\'' +
                '}'+"\n\n";
    }
}
