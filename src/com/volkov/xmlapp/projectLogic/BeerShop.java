package com.volkov.xmlapp.projectLogic;


import java.util.ArrayList;
import java.util.List;

public class BeerShop {
    private List<Beer> beerList;
    public BeerShop(){
        beerList = new ArrayList();
    }

    public List<Beer> getBeerList() {
        return beerList;
    }

    @Override
    public String toString() {
        return "BeerShop{" +
                "beerList=" + beerList +
                '}';
    }
}
