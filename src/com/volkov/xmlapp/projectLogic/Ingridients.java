package com.volkov.xmlapp.projectLogic;


import java.util.ArrayList;
import java.util.List;

public class Ingridients {

    private List<String> ingrList;

    public Ingridients(){
        ingrList = new ArrayList<>();
    }

    public Ingridients(String[] ingr){
        ingrList = new ArrayList<>();
        for (int i =0; i<ingr.length;i++){
            ingrList.add(ingr[i]);
        }
    }

    public List<String> getIngrList() {
        return ingrList;
    }

    @Override
    public String toString() {
        return "Ingridients{" +
                "ingrList=" + ingrList +
                '}';
    }
}
