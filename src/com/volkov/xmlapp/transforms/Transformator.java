package com.volkov.xmlapp.transforms;


import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class Transformator {

    TransformerFactory transformerFactory;

    public Transformator(){
         transformerFactory =  TransformerFactory.newInstance();
    }

    public void transformMeth(String xslLink, String xmlToTransform, String resulitXML){
        try {
            Transformer transformer = transformerFactory.newTransformer(new StreamSource(xslLink));
            transformer.transform(new StreamSource(xmlToTransform),new StreamResult(resulitXML));
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        System.out.println("GOTOVO!");
    }

}
